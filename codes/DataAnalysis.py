import pandas as pd
import numpy as np
import random as rnd
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC, LinearSVC
from sklearn.ensemble import RandomForestClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.linear_model import Perceptron
from sklearn.linear_model import SGDClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.cross_validation import train_test_split, KFold
from sklearn.metrics import make_scorer, accuracy_score
from sklearn.model_selection import GridSearchCV

train_df = pd.read_csv('YOUR_PATH_OS2/input/train.csv')
test_df = pd.read_csv('YOUR_PATH_OS2/input/test.csv')

train_df = train_df.drop(['Ticket', 'Cabin'], axis=1)
test_df = test_df.drop(['Ticket', 'Cabin'], axis=1)
combine = [train_df, test_df]

for dataset in combine:
    dataset['Title'] = dataset.Name.str.extract(' ([A-Za-z]+)\.', expand=False)

print('Crosstab Title & Sex : ')
print(pd.crosstab(train_df['Title'], train_df['Sex']))
input("Press Enter to continue...")

print('Grouping Title...')
for dataset in combine:
    dataset['Title'] = dataset['Title'].replace(['Lady', 'Countess','Capt', 'Col',\
    'Don', 'Dr', 'Major', 'Rev', 'Sir', 'Jonkheer', 'Dona'], 'Rare')

    dataset['Title'] = dataset['Title'].replace('Mlle', 'Miss')
    dataset['Title'] = dataset['Title'].replace('Ms', 'Miss')
    dataset['Title'] = dataset['Title'].replace('Mme', 'Mrs')

print('Grouped Title Mean')
print(train_df[['Title', 'Survived']].groupby(['Title'], as_index=False).mean())
input("Press Enter to continue...")

print('Mapping Title to Ordinal Data...')
title_mapping = {"Mr": 1, "Miss": 2, "Mrs": 3, "Master": 4, "Rare": 5}

for dataset in combine:
    dataset['Title'] = dataset['Title'].map(title_mapping)
    dataset['Title'] = dataset['Title'].fillna(0)

print('Mapped Train Data Head : ')
print(train_df.head())
input("Press Enter to continue...")

#drop after used
train_df = train_df.drop(['Name', 'PassengerId'], axis=1)
test_df = test_df.drop(['Name'], axis=1)
combine = [train_df, test_df]

print('Current Train & Test Data Shape :')
print(train_df.shape, test_df.shape)

print('Converting Sex Feature...')

for dataset in combine:
    dataset['Sex'] = dataset['Sex'].map( {'female': 1, 'male': 0} ).astype(int)

print('Completing Age Feature...')
guess_ages = np.zeros((2,3))

for dataset in combine:
    for i in range(0, 2):
        for j in range(0, 3):
            guess_df = dataset[(dataset['Sex'] == i) & \
                               (dataset['Pclass'] == j + 1)]['Age'].dropna()

            age_guess = guess_df.median()

            # Convert random age float to nearest .5 age
            guess_ages[i, j] = int(age_guess / 0.5 + 0.5) * 0.5

    for i in range(0, 2):
        for j in range(0, 3):
            dataset.loc[(dataset.Age.isnull()) & (dataset.Sex == i) & (dataset.Pclass == j + 1), \
                        'Age'] = guess_ages[i, j]

    dataset['Age'] = dataset['Age'].astype(int)

print('Generating Group for Age...')
train_df['AgeBand'] = pd.cut(train_df['Age'], 5)
print(train_df[['AgeBand', 'Survived']].groupby(['AgeBand'], as_index=False).mean().sort_values(by='AgeBand', ascending=True))
input("Press Enter to continue...")

for dataset in combine:
    dataset.loc[ dataset['Age'] <= 16, 'Age'] = 0
    dataset.loc[(dataset['Age'] > 16) & (dataset['Age'] <= 32), 'Age'] = 1
    dataset.loc[(dataset['Age'] > 32) & (dataset['Age'] <= 48), 'Age'] = 2
    dataset.loc[(dataset['Age'] > 48) & (dataset['Age'] <= 64), 'Age'] = 3
    dataset.loc[ dataset['Age'] > 64, 'Age']

train_df = train_df.drop(['AgeBand'], axis=1)
combine = [train_df, test_df]

for dataset in combine:
    dataset['FamilySize'] = dataset['SibSp'] + dataset['Parch'] + 1

train_df[['FamilySize', 'Survived']].groupby(['FamilySize'], as_index=False).mean().sort_values(by='Survived', ascending=False)

for dataset in combine:
    dataset['IsAlone'] = 0
    dataset.loc[dataset['FamilySize'] == 1, 'IsAlone'] = 1

print(train_df[['IsAlone', 'Survived']].groupby(['IsAlone'], as_index=False).mean())
input("Press Enter to continue...")

train_df = train_df.drop(['Parch', 'SibSp', 'FamilySize'], axis=1)
test_df = test_df.drop(['Parch', 'SibSp', 'FamilySize'], axis=1)
combine = [train_df, test_df]

for dataset in combine:
    dataset['Age*Class'] = dataset.Age * dataset.Pclass

train_df.loc[:, ['Age*Class', 'Age', 'Pclass']].head(10)

# Completing Categorical Feature, By Most Occurance
freq_port = train_df.Embarked.dropna().mode()[0]

for dataset in combine:
    dataset['Embarked'] = dataset['Embarked'].fillna(freq_port)

print(train_df[['Embarked', 'Survived']].groupby(['Embarked'], as_index=False).mean().sort_values(by='Survived', ascending=False))
input("Press Enter to continue...")

for dataset in combine:
    dataset['Embarked'] = dataset['Embarked'].map( {'S': 0, 'C': 1, 'Q': 2} ).astype(int)

# Completing single missing value
test_df['Fare'].fillna(test_df['Fare'].dropna().median(), inplace=True)

train_df['FareBand'] = pd.qcut(train_df['Fare'], 4)
print(train_df[['FareBand', 'Survived']].groupby(['FareBand'], as_index=False).mean().sort_values(by='FareBand', ascending=True))
input("Press Enter to continue...")

for dataset in combine:
    dataset.loc[ dataset['Fare'] <= 7.91, 'Fare'] = 0
    dataset.loc[(dataset['Fare'] > 7.91) & (dataset['Fare'] <= 14.454), 'Fare'] = 1
    dataset.loc[(dataset['Fare'] > 14.454) & (dataset['Fare'] <= 31), 'Fare']   = 2
    dataset.loc[ dataset['Fare'] > 31, 'Fare'] = 3
    dataset['Fare'] = dataset['Fare'].astype(int)

train_df = train_df.drop(['FareBand'], axis=1)
combine = [train_df, test_df]

print('Final Train Data Head : ')
print(train_df.head(10))

print('Final Test Data Head : ')
print(test_df.head(10))

# Modeling Data
X_train = train_df.drop("Survived", axis=1)
Y_train = train_df["Survived"]
X_test  = test_df.drop("PassengerId", axis=1).copy()

X_train , X_valid , Y_train , Y_valid = train_test_split(X_train, Y_train, train_size = .7)
print('CV set')
print (X_train.shape, X_valid.shape, Y_train.shape, Y_valid.shape, X_test.shape)
input("Press Enter to continue...")

print("Training Data...")

logreg = LogisticRegression()
logreg.fit(X_train, Y_train)
acc_train_log = round(logreg.score(X_train, Y_train) * 100, 2)
acc_cv_log = round(logreg.score(X_valid, Y_valid) * 100, 2)

# print('Logistic Regression Accuracy : ' + acc_log)

# Check features correlation
coeff_df = pd.DataFrame(train_df.columns.delete(0))
coeff_df.columns = ['Feature']
coeff_df["Correlation"] = pd.Series(logreg.coef_[0])

print('Highest Correlation by Logistic Regression : ')
print(coeff_df.sort_values(by='Correlation', ascending=False))

svc = SVC()
svc.fit(X_train, Y_train)
# Y_pred = svc.predict(X_test)
acc_train_svc = round(svc.score(X_train, Y_train) * 100, 2)
acc_cv_svc = round(svc.score(X_valid, Y_valid) * 100, 2)

knn = KNeighborsClassifier(n_neighbors = 3)
knn.fit(X_train, Y_train)
# Y_pred = knn.predict(X_test)
acc_train_knn = round(knn.score(X_train, Y_train) * 100, 2)
acc_cv_knn = round(knn.score(X_valid, Y_valid) * 100, 2)

gaussian = GaussianNB()
gaussian.fit(X_train, Y_train)
# Y_pred = gaussian.predict(X_test)
acc_train_gaussian = round(gaussian.score(X_train, Y_train) * 100, 2)
acc_cv_gaussian = round(gaussian.score(X_valid, Y_valid) * 100, 2)

perceptron = Perceptron()
perceptron.fit(X_train, Y_train)
# Y_pred = perceptron.predict(X_test)
acc_train_perceptron = round(perceptron.score(X_train, Y_train) * 100, 2)
acc_cv_perceptron = round(perceptron.score(X_valid, Y_valid) * 100, 2)

linear_svc = LinearSVC()
linear_svc.fit(X_train, Y_train)
# Y_pred = linear_svc.predict(X_test)
acc_train_linear_svc = round(linear_svc.score(X_train, Y_train) * 100, 2)
acc_cv_linear_svc = round(linear_svc.score(X_valid, Y_valid) * 100, 2)

sgd = SGDClassifier()
sgd.fit(X_train, Y_train)
# Y_pred = sgd.predict(X_test)
acc_train_sgd = round(sgd.score(X_train, Y_train) * 100, 2)
acc_cv_sgd = round(sgd.score(X_valid, Y_valid) * 100, 2)

decision_tree = DecisionTreeClassifier()
decision_tree.fit(X_train, Y_train)
# Y_pred = decision_tree.predict(X_test)
acc_train_decision_tree = round(decision_tree.score(X_train, Y_train) * 100, 2)
acc_cv_decision_tree = round(decision_tree.score(X_valid, Y_valid) * 100, 2)

random_forest = RandomForestClassifier(n_estimators=100)
random_forest.fit(X_train, Y_train)
# Y_pred = random_forest.predict(X_test)
acc_train_random_forest = round(random_forest.score(X_train, Y_train) * 100, 2)
acc_cv_random_forest = round(random_forest.score(X_valid, Y_valid) * 100, 2)

# Choose some parameter combinations to try
parameters = {'n_estimators': [100, 200, 500],
              'max_features': ['log2', 'sqrt','auto'],
              'criterion': ['entropy', 'gini'],
              'min_samples_leaf': [1,5,10,50,100],
              'oob_score': [True, False],
              'n_jobs': [-1]
             }

# Choose the type of classifier.
clf = RandomForestClassifier(verbose=True)

# Type of scoring used to compare parameter combinations
acc_scorer = make_scorer(accuracy_score)

# Run the grid search
grid_obj = GridSearchCV(clf, parameters, scoring=acc_scorer)
grid_obj = grid_obj.fit(X_train, Y_train)

# Set the clf to the best combination of parameters
clf = grid_obj.best_estimator_

# Fit the best algorithm to the data.
clf.fit(X_train, Y_train)

acc_train_clf = round(clf.score(X_train, Y_train) * 100, 2)
acc_cv_clf = round(clf.score(X_valid, Y_valid) * 100, 2)

print("Running KFold...")
#KFold
kf = KFold(891, n_folds=10)
outcomes = []
fold = 0
for train_index, test_index in kf:
    fold += 1
    clf.fit(X_train, Y_train)
    predictions = clf.predict(X_valid)
    accuracy = accuracy_score(Y_valid, predictions)
    outcomes.append(accuracy)
    print("Fold {0} accuracy: {1}".format(fold, accuracy))
mean_outcome = np.mean(outcomes)
print("Mean Accuracy: {0}".format(mean_outcome))
input("Press Enter to continue...")

models = pd.DataFrame({
    'Model': ['Support Vector Machines', 'KNN', 'Logistic Regression',
              'Random Forest', 'Naive Bayes', 'Perceptron',
              'Stochastic Gradient Decent', 'Linear SVC',
              'Decision Tree', 'Custom Random Forest'],
    'TrainScore': [acc_train_svc, acc_train_knn, acc_train_log,
              acc_train_random_forest, acc_train_gaussian, acc_train_perceptron,
              acc_train_sgd, acc_train_linear_svc, acc_train_decision_tree, acc_train_clf],
    'CVScore': [acc_cv_svc, acc_cv_knn, acc_cv_log,
                    acc_cv_random_forest, acc_cv_gaussian, acc_cv_perceptron,
                    acc_cv_sgd, acc_cv_linear_svc, acc_cv_decision_tree, acc_cv_clf]})

print('Best Model Result : ')
print(models.sort_values(by='CVScore', ascending=False))