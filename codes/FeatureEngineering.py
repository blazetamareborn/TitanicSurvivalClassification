import numpy as np
import pandas as pd

from GenMissingValues import gen_missing_value


def feature_engineering() :
    train, test = gen_missing_value()

    combine = pd.concat([train.drop('Survived',1),test])
    survived = train['Survived']

    combine['Child'] = combine['Age']<=10
    combine['Cabin_known'] = combine['Cabin'].isnull() == False
    combine['Age_known'] = combine['Age'].isnull() == False
    # combine['Family'] = combine['SibSp'] + combine['Parch']
    combine['Alone']  = (combine['SibSp'] + combine['Parch']) == 0
    combine['Large_Family'] = (combine['SibSp']>2) | (combine['Parch']>3)
    combine['Deck'] = combine['Cabin'].str[0]
    combine['Deck'] = combine['Deck'].fillna(value='U')
    combine['Ttype'] = combine['Ticket'].str[0]
    combine['Title'] = combine['Name'].str.split(", ", expand=True)[1].str.split(".", expand=True)[0]
    combine['Fare_cat'] = pd.DataFrame(np.floor(np.log10(combine['Fare'] + 1))).astype('int')
    combine['Bad_ticket'] = combine['Ttype'].isin(['3','4','5','6','7','8','A','L','W'])
    combine['Young'] = (combine['Age']<=30) | (combine['Title'].isin(['Master','Miss','Mlle']))
    combine['Shared_ticket'] = np.where(combine.groupby('Ticket')['Name'].transform('count') > 1, 1, 0)
    combine['Ticket_group'] = combine.groupby('Ticket')['Name'].transform('count')
    combine['Fare_eff'] = combine['Fare']/combine['Ticket_group']
    combine['Fare_eff_cat'] = np.where(combine['Fare_eff']>16.0, 2, 1)
    combine['Fare_eff_cat'] = np.where(combine['Fare_eff']<8.5,0,combine['Fare_eff_cat'])

    combine['Age'].fillna(combine['Age'].dropna().median(), inplace=True)

    test = combine.iloc[len(train):]
    train = combine.iloc[:len(train)]
    train['Survived'] = survived

    return train, test




