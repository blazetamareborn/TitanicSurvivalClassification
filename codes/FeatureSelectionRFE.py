import pandas as pd
import numpy as np
from sklearn.linear_model import LogisticRegression
from sklearn.feature_selection import RFECV

def feature_selection_rfe(training):
    X = training
    X = X.drop(['Survived'], axis=1)
    y = training['Survived']

    best_param_logreg = {
        'max_iter': 1000,
        'penalty': 'l2',
        'C': 0.1,
        'random_state': 0
    }

    clf = LogisticRegression(**best_param_logreg)
    rfe = RFECV(estimator=clf, step=1, cv=10)
    rfe.fit(X, y)

    result = sorted(zip(map(lambda x: round(x, 4), rfe.ranking_), X.columns.values))

    print("Features sorted by their rank:")
    print(result)

    # select top 10?
    # return result[:10]
