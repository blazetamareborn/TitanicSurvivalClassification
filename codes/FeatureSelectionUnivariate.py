import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.feature_selection import SelectKBest, chi2, f_classif

def feature_selection(training):
    X = training
    X = X.drop(['Survived'], axis=1)
    y = training['Survived']

    k_best_result = SelectKBest(chi2, k=10)
    k_best_result_fit = k_best_result.fit(X, y)
    k_best_result_fit_trans = k_best_result.fit_transform(X, y)
    mask = k_best_result_fit.get_support()
    # training_filtered_chi = X.columns[mask]
    filtered_features = []

    for bool, feature in zip(mask, X.columns.values):
        if bool:
            filtered_features.append(feature)

    filtered_features_df = pd.DataFrame(k_best_result_fit_trans, columns=filtered_features)

    return filtered_features_df
