import pandas as pd
from CheckOS import is_windows
from DetectOutlier import detect_outliers

def gen_missing_value():
    if is_windows() == 1:
        train = pd.read_csv('YOUR_PATH_OS1/input/train.csv')
        test = pd.read_csv('YOUR_PATH_OS1/input/test.csv')
    else:
        train = pd.read_csv('YOUR_PATH_OS2/input/train.csv')
        test = pd.read_csv('YOUR_PATH_OS2/input/test.csv')
    combine = pd.concat([train.drop('Survived',1),test])

    train['Embarked'].iloc[61] = "C"
    train['Embarked'].iloc[829] = "C"

    test['Fare'].iloc[152] = combine['Fare'][combine['Pclass'] == 3].dropna().median()
    # print(test['Fare'].iloc[152])

    # drop outliers
    outliers_to_drop = detect_outliers(train, 2, ["Age", "SibSp", "Parch", "Fare"])
    train = train.drop(outliers_to_drop, axis=0).reset_index(drop=True)

    return train, test