import matplotlib.pyplot as plt
import numpy as np
from sklearn.model_selection import validation_curve

def gen_validation_curve(estimator, X, y) :
    param_range = [0.001, 0.01, 0.1, 1, 10, 100, 500, 1000]
    train_scores, test_scores = validation_curve(
                    estimator=estimator,
                    X=X,
                    y=y,
                    param_name='C',
                    param_range=param_range,
                    cv=10)

    train_mean = np.mean(train_scores, axis=1)
    train_std = np.std(train_scores, axis=1)
    test_mean = np.mean(test_scores, axis=1)
    test_std = np.std(test_scores, axis=1)

    plt.plot(param_range, train_mean,
             color='blue', marker='o',
             markersize=5, label='training accuracy')

    plt.fill_between(param_range, train_mean + train_std,
                     train_mean - train_std, alpha=0.15,
                     color='blue')

    plt.plot(param_range, test_mean,
             color='green', linestyle='--',
             marker='s', markersize=5,
             label='validation accuracy')

    plt.fill_between(param_range,
                     test_mean + test_std,
                     test_mean - test_std,
                     alpha=0.15, color='green')

    plt.grid()
    plt.xscale('log')
    plt.legend(loc='lower right')
    plt.xlabel('Parameter C')
    plt.ylabel('Accuracy')
    plt.ylim([0.0, 1.0])
    plt.tight_layout()
    # plt.savefig('images/06_06.png', dpi=300)
    plt.show()