import numpy as np
import pandas as pd
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split

from FeatureEngineering import feature_engineering
from FeatureSelectionUnivariate import feature_selection


def model_preparation() :
    train, test = feature_engineering()

    pd.set_option('display.max_columns', None)

    # region convert_features_to_int
    combine = pd.concat([train.drop('Survived',1),test]) # Dataframe
    survived = train['Survived']

    # combine = combine.drop(['PassengerId'], axis=1)
    combine = combine.drop(['Name'], axis=1)
    combine = combine.drop(['Ticket'], axis=1)
    combine = combine.drop(['Ttype'], axis=1)
    combine = combine.drop(['Fare'], axis=1)
    combine = combine.drop(['Fare_eff'], axis=1)
    combine = combine.drop(['Cabin'], axis=1)
    combine = combine.drop(['SibSp'], axis=1)
    combine = combine.drop(['Parch'], axis=1)

    combine["Sex"] = combine["Sex"].astype("category")
    combine["Sex"].cat.categories = [0,1]
    combine["Sex"] = combine["Sex"].astype("int")

    # combine["Embarked"] = combine["Embarked"].astype("category")
    # combine["Embarked"].cat.categories = [0,1,2]
    # combine["Embarked"] = combine["Embarked"].astype("int")

    # combine["Deck"] = combine["Deck"].astype("category")
    # combine["Deck"].cat.categories = [0,1,2,3,4,5,6,7,8]
    # combine["Deck"] = combine["Deck"].astype("int")

    combine["Child"] = combine["Child"].astype("category")
    combine["Child"].cat.categories = [0,1]
    combine["Child"] = combine["Child"].astype("int")

    combine["Cabin_known"] = combine["Cabin_known"].astype("category")
    combine["Cabin_known"].cat.categories = [0,1]
    combine["Cabin_known"] = combine["Cabin_known"].astype("int")

    combine["Cabin_known"] = combine["Cabin_known"].astype("category")
    combine["Cabin_known"].cat.categories = [0,1]
    combine["Cabin_known"] = combine["Cabin_known"].astype("int")

    combine["Age_known"] = combine["Age_known"].astype("category")
    combine["Age_known"].cat.categories = [0,1]
    combine["Age_known"] = combine["Age_known"].astype("int")

    combine["Alone"] = combine["Alone"].astype("category")
    combine["Alone"].cat.categories = [0,1]
    combine["Alone"] = combine["Alone"].astype("int")

    combine["Large_Family"] = combine["Large_Family"].astype("category")
    combine["Large_Family"].cat.categories = [0,1]
    combine["Large_Family"] = combine["Large_Family"].astype("int")

    # range dpt dari AgeBand (DataAnalysis.py)
    combine.loc[combine['Age'] <= 16, 'Age'] = 0
    combine.loc[(combine['Age'] > 16) & (combine['Age'] <= 32), 'Age'] = 1
    combine.loc[(combine['Age'] > 32) & (combine['Age'] <= 48), 'Age'] = 2
    combine.loc[(combine['Age'] > 48) & (combine['Age'] <= 64), 'Age'] = 3
    combine.loc[combine['Age'] > 64, 'Age'] = 4

    combine['Title'] = combine['Title'].replace(['Lady', 'Countess','Capt', 'Col',\
    'Don', 'Dr', 'Major', 'Rev', 'Sir', 'Jonkheer', 'Dona', 'the Countess'], 'Rare')

    combine['Title'] = combine['Title'].replace('Mlle', 'Miss')
    combine['Title'] = combine['Title'].replace('Ms', 'Miss')
    combine['Title'] = combine['Title'].replace('Mme', 'Mrs')
    # combine['Title'] = combine['Title'].fillna('NA')

    # title_mapping = {"Mr": 1, "Miss": 2, "Mrs": 3, "Master": 4, "Rare": 5}
    # combine['Title'] = combine['Title'].map(title_mapping)
    # combine['Title'] = combine['Title'].fillna(0)

    combine["Bad_ticket"] = combine["Bad_ticket"].astype("category")
    combine["Bad_ticket"].cat.categories = [0,1]
    combine["Bad_ticket"] = combine["Bad_ticket"].astype("int")

    combine["Young"] = combine["Young"].astype("category")
    combine["Young"].cat.categories = [0,1]
    combine["Young"] = combine["Young"].astype("int")
    # endregion

    # region one_hot_encoder
    embarked = pd.get_dummies(combine.Embarked, prefix='Embarked')
    deck = pd.get_dummies(combine.Deck, prefix='Deck')
    title = pd.get_dummies(combine.Title, prefix='Title')
    ticket_group = pd.get_dummies(combine.Ticket_group, prefix='Ticket_Group')
    # ordinal
    # age = pd.get_dummies(combine.Age, prefix='Age_Group')
    # pclass = pd.get_dummies(combine.Pclass, prefix='Pclass')
    # fare_cat = pd.get_dummies(combine.Fare_cat, prefix='Fare_Cat')
    # fare_eff_cat = pd.get_dummies(combine.Fare_eff_cat, prefix='Fare_Eff_Cat')

    combine = pd.concat([combine, embarked, deck, title, ticket_group], axis=1)
    combine = combine.drop(['Embarked', 'Deck', 'Title', 'Ticket_group'], axis=1)
    # endregion

    test = combine.iloc[len(train):]
    train = combine.iloc[:len(train)]
    train['Survived'] = survived

    # train.loc[:,["Sex","Embarked"]].head()

    # ax = plt.subplots(figsize=(12, 10))
    # foo = sns.heatmap(train.drop('PassengerId', axis=1).corr(), vmax=1.0, square=True, annot=True)

    training, testing = train_test_split(train, test_size=0.2, stratify=train['Survived'], random_state=0)
    # print("Total sample size = %i; training sample size = %i, testing sample size = %i" \
    #     % (train.shape[0], training.shape[0], testing.shape[0]))

    # Gk perlu dipanggil, run manual aja?
    # training = feature_selection(training)

    # input("Press Enter to continue...")

    # region use_picked_features (Top 10 RFECV)
    cols = ['Bad_ticket', 'Cabin_known', 'Large_Family', 'Sex', 'Title_Master', 'Title_Miss', 'Title_Mr', 'Title_Mrs',
            'Fare_eff_cat', 'Ticket_Group_4']
    tcols = np.append(['Survived'], cols)

    df = training.loc[:, tcols].dropna()
    X = df.loc[:, cols]
    y = df.loc[:, ['Survived']]

    df_test = testing.loc[:, tcols].dropna()
    X_test = df_test.loc[:, cols]
    y_test = df_test.loc[:, ['Survived']]
    # endregion

    # region use_all_features
    '''
    cols = ['']

    X = training
    X = X.drop(['Survived'], axis=1)
    y = training['Survived']

    X_test = testing
    X_test = X_test.drop(['Survived'], axis=1)
    y_test = testing['Survived']
    '''
    # endregion

    return train, test, X, y, X_test, y_test, cols

