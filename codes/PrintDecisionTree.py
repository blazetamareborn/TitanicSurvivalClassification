from pydotplus import graph_from_dot_data
from sklearn import tree

def print_decision_tree(clf, X):
    dot_data = tree.export_graphviz(clf,
            filled=True,
            rounded=True,
            class_names=['0', '1'],
            feature_names=X.columns.values,
            out_file=None)

    graph = graph_from_dot_data(dot_data)
    graph.write_png('YOUR_PATH_OS2/output/tree.png')