import pandas as pd
from CheckOS import is_windows

def submit_kaggle(passangers_id, y_pred) :
    submission = pd.DataFrame({
            "PassengerId": passangers_id,
            "Survived": y_pred
        })
    if is_windows()==1:
        submission.to_csv('YOUR_PATH_OS1/output/submission.csv', index=False)
    else:
        submission.to_csv('YOUR_PATH_OS2/output/submission.csv', index=False)