import pandas as pd
from sklearn.svm import SVC
from sklearn import tree
import sys
sys.path.append("YOUR_PATH_OS1/codes")
sys.path.append("YOUR_PATH_OS2/codes")
from ModelPreparation import model_preparation
from SubmitKaggle import submit_kaggle

train, test, X, y, X_test, y_test, cols = model_preparation()

X = pd.concat([X,X_test])
y = pd.concat([y,y_test])

parameters = {'max_depth': 6,
              'max_features': 'sqrt',
              'presort': [True],
              'random_state': 0}

clf = tree.DecisionTreeClassifier(**parameters)
clf.fit(X, y)
score = clf.score(X, y)

print('Accuracy : ' + str(score))

y_pred = clf.predict(test)

submit_kaggle(test.loc[:,'PassengerId'], y_pred)
