import pandas as pd
from sklearn.ensemble import RandomForestClassifier
import sys
sys.path.append("YOUR_PATH_OS1/codes")
sys.path.append("YOUR_PATH_OS2/codes")
from ModelPreparation import model_preparation
from SubmitKaggle import submit_kaggle

train, test, X, y, X_test, y_test, cols = model_preparation()

X = pd.concat([X,X_test])
y = pd.concat([pd.DataFrame(y),pd.DataFrame(y_test)])

parameters = {
                'n_estimators': 100,
                # 'criterion': 'entropy',
                'oob_score': True, # untuk scoring saja, gak ngefek ke performance
                'min_samples_leaf': 3,
                # 'min_weight_fraction_leaf': [0.0, 0.01, 0.1],
                'max_depth': 6,
                'class_weight': 'balanced',
                'max_features': 'log2',
                'n_jobs': -1,
                'random_state': 0
             }

clf = RandomForestClassifier(**parameters)
clf.fit(X, y)
score = clf.score(X, y)

print('Accuracy : ' + str(score))
print("OOB Score : " + str(clf.oob_score_))

# use selected features
test_filtered = test.loc[:, cols].fillna(method='pad')
y_pred = clf.predict(test_filtered)

# use all features
# y_pred = clf_voting_hard.predict(test)

submit_kaggle(test.loc[:,'PassengerId'], y_pred)
