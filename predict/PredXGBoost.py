import pandas as pd
from xgboost.sklearn import XGBClassifier
from sklearn.metrics import make_scorer, accuracy_score, f1_score
import sys
sys.path.append("YOUR_PATH_OS1/codes")
sys.path.append("YOUR_PATH_OS2/codes")
from ModelPreparation import model_preparation
from SubmitKaggle import submit_kaggle

train, test, X, y, X_test, y_test, cols = model_preparation()

X = pd.concat([X,X_test])
y = pd.concat([y,y_test])

parameters = {
                'n_estimators': 100, # default 100
                'max_depth': 3,
                'gamma': 0,
                'subsample': 0.8,
                'reg_alpha': 1, # L1 regularization, default 0
                'reg_lambda': 1, # L2 regularization, default 1
                # 'learning_rate': [0.1, 0.3], # biar lebih cepat
                'nthread': -1,
                'seed': 0
             }

clf = XGBClassifier(**parameters)
clf.fit(X, y)

score = clf.score(X, y)
# y_pred_test = clf.predict(X_test)

# print('Accuracy : ' + str(accuracy_score(y_test, y_pred_test)))
# print('F1 Score : ' + str(f1_score(y_test, y_pred_test)))

y_pred = clf.predict(test)

submit_kaggle(test.loc[:,'PassengerId'], y_pred)
