# region imports
# for seaborn issue:
import warnings
warnings.filterwarnings("ignore")

import pandas as pd
import numpy as np

from sklearn.linear_model import LogisticRegression
from sklearn.linear_model import Perceptron
from sklearn import tree
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.ensemble import AdaBoostClassifier
from sklearn.ensemble import GradientBoostingClassifier
from sklearn import svm
import xgboost as xgb
# from mlxtend.classifier import StackingClassifier

from sklearn.model_selection import cross_val_score

import sys
# sys.path.insert(0, "../codes")
sys.path.append("YOUR_PATH_OS1/codes")
# sys.path.append('../codes')
from ModelPreparation import model_preparation
from SubmitKaggle import submit_kaggle
# endregion

train, test, X, y, X_test, y_test, cols = model_preparation()

# region train_basic_models
clf_log = LogisticRegression()
clf_log = clf_log.fit(X,y)
scores_log = cross_val_score(clf_log, X, y, cv=5)
score_log_train = np.mean(scores_log)
std_log_train = np.std(scores_log)
score_log_test = clf_log.score(X_test,y_test)
# print('Logistic Regression (Mean) : ' + str(np.mean(scores_log)) + ', STD : ' + str(np.std(scores_log)))

clf_pctr = Perceptron(
    class_weight='balanced'
    )
clf_pctr = clf_pctr.fit(X,y)
scores_pctr = cross_val_score(clf_pctr, X, y, cv=5)
score_pctr_train = np.mean(scores_pctr)
std_pctr_train = np.std(scores_pctr)
score_pctr_test = clf_pctr.score(X_test,y_test)
# print('Perceptron (Mean) : ' + str(np.mean(scores_pctr)) + ', STD : ' + str(np.std(scores_pctr)))

clf_knn = KNeighborsClassifier(
    n_neighbors=10,
    weights='distance'
    )
clf_knn = clf_knn.fit(X,y)
scores_knn = cross_val_score(clf_knn, X, y, cv=5)
score_knn_train = np.mean(scores_knn)
std_knn_train = np.std(scores_knn)
score_knn_test = clf_knn.score(X_test,y_test)
# print('KNN (Mean) : ' + str(np.mean(scores_knn)) + ', STD : ' + str(np.std(scores_knn)))

clf_svm = svm.SVC(
    class_weight='balanced'
    )
clf_svm.fit(X, y)
scores_svm = cross_val_score(clf_svm, X, y, cv=5)
score_svm_train = np.mean(scores_svm)
std_svm_train = np.std(scores_svm)
score_svm_test = clf_svm.score(X_test,y_test)
# print('SVM (Mean) : ' + str(np.mean(scores_svm)) + ', STD : ' + str(np.std(scores_svm)))

clf_tree = tree.DecisionTreeClassifier(
    # max_depth=3,\
    # class_weight="balanced",\
    # min_weight_fraction_leaf=0.01\
    )
clf_tree = clf_tree.fit(X,y)
scores_tree = cross_val_score(clf_tree, X, y, cv=5)
score_tree_train = np.mean(scores_tree)
std_tree_train = np.std(scores_tree)
score_tree_test = clf_tree.score(X_test,y_test)
# print('Decision Tree (Mean) : ' + str(np.mean(scores_tree)) + ', STD : ' + str(np.std(scores_tree)))

clf_rf = RandomForestClassifier(
    n_estimators=1000, \
    max_depth=None, \
    min_samples_split=10 \
    #class_weight="balanced", \
    #min_weight_fraction_leaf=0.02 \
    )
clf_rf = clf_rf.fit(X,y)
scores_rf = cross_val_score(clf_rf, X, y, cv=5)
score_rf_train = np.mean(scores_rf)
std_rf_train = np.std(scores_rf)
score_rf_test = clf_rf.score(X_test,y_test)
# print('Random Forest (Mean) : ' + str(np.mean(scores_rf)) + ', STD : ' + str(np.std(scores_rf)))

clf_ext = ExtraTreesClassifier(
    max_features='auto',
    bootstrap=True,
    oob_score=True,
    n_estimators=1000,
    max_depth=None,
    min_samples_split=10
    #class_weight="balanced",
    #min_weight_fraction_leaf=0.02
    )
clf_ext = clf_ext.fit(X,y)
scores_ext = cross_val_score(clf_ext, X, y, cv=5)
score_ext_train = np.mean(scores_ext)
std_ext_train = np.std(scores_ext)
score_ext_test = clf_ext.score(X_test,y_test)
# print('Extra Trees (Mean) : ' + str(np.mean(scores_ext)) + ', STD : ' + str(np.std(scores_ext)))

clf_gb = GradientBoostingClassifier(
            #loss='exponential',
            n_estimators=1000,
            learning_rate=0.1,
            max_depth=3,
            subsample=0.5,
            random_state=0).fit(X, y)
clf_gb.fit(X,y)
scores_gb = cross_val_score(clf_gb, X, y, cv=5)
score_gb_train = np.mean(scores_gb)
std_gb_train = np.std(scores_gb)
score_gb_test = clf_gb.score(X_test,y_test)
# print('Gradient Boosting (Mean) : ' + str(np.mean(score_gb)) + ', STD : ' + str(np.std(score_gb)))

clf_ada = AdaBoostClassifier(n_estimators=400, learning_rate=0.1)
clf_ada.fit(X,y)
scores_ada = cross_val_score(clf_ada, X, y, cv=5)
score_ada_train = np.mean(scores_ada)
std_ada_train = np.std(scores_ada)
score_ada_test = clf_ada.score(X_test,y_test)
# print('ADA Boost (Mean) : ' + str(np.mean(scores_ada)) + ', STD : ' + str(np.std(scores_ada)))

clf_xgb = xgb.XGBClassifier(
    max_depth=2,
    n_estimators=500,
    subsample=0.5,
    learning_rate=0.1
    )
clf_xgb.fit(X,y)
scores_xgb = cross_val_score(clf_xgb, X, y, cv=5)
score_xgb_train = np.mean(scores_xgb)
std_xgb_train = np.std(scores_xgb)
score_xgb_test = clf_xgb.score(X_test,y_test)
# print('Xtra Gradient Boosting : ' + scores_xgb)
# endregion

models = pd.DataFrame({
    'Model': ['Support Vector Machines', 'KNN', 'Logistic Regression',
              'Random Forest', 'Gradient Boosting',
              'Decision Tree','XGBoost','ExtraTree','Perceptron'],
    'Train_Score': [score_svm_train, score_knn_train, score_log_train, score_rf_train, score_gb_train,
              score_tree_train,score_xgb_train,score_ext_train,score_pctr_train],
    'Train_STD': [std_svm_train, std_knn_train, std_log_train, std_rf_train, std_gb_train,
                    std_tree_train, std_xgb_train, std_ext_train, std_pctr_train],
    'Test_Score': [score_svm_test, score_knn_test, score_log_test, score_rf_test, score_gb_test,
                    score_tree_test, score_xgb_test, score_ext_test, score_pctr_test]
})

print(models.sort_values(by='Test_Score', ascending=False))
# endregion
