# region imports
import pandas as pd
import numpy as np
from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn import tree
from xgboost.sklearn import XGBClassifier
from sklearn.linear_model import Perceptron
from sklearn.neural_network import MLPClassifier
from sklearn.metrics import make_scorer, accuracy_score, f1_score
from sklearn.model_selection import GridSearchCV, cross_val_score
from sklearn.metrics import f1_score
import sys
sys.path.append("YOUR_PATH_OS1/codes")
sys.path.append("YOUR_PATH_OS2/codes")
from ModelPreparation import model_preparation
from GenLearningCurve import gen_learning_curve
from GenValidationCurve import gen_validation_curve
from SubmitKaggle import submit_kaggle
# from PrintDecisionTree import print_decision_tree
# endregion

train, test, X, y, X_test, y_test, cols = model_preparation()

# fit with all data
# X = pd.concat([X,X_test])
# y = pd.concat([y,y_test])

y = y.iloc[:,0]

# logreg
'''
parameters = {
                'max_iter': [1000],
                'penalty': ['l1', 'l2'],
                'C': [0.1, 1, 3, 10, 100],
                # 'n_jobs': [-1], #liblinear gk ngefek
                'random_state': [0]
             }

clf = LogisticRegression()
'''

# SVM
# best : C=3000, gamma=6e-06 -> Test Acc 0.804469273743, tp Kaggle 0.51674?
'''
parameters = {
                'C': [1, 10, 50, 100,200,300, 1000], # makin kecil, makin besar margin
                'gamma': [0.001, 0.01, 0.1, 1],
                'class_weight': [None, 'balanced'],
                'probability': [True],
                'random_state': [0]
             }

clf = SVC()
'''

# KNN
'''
parameters = {
                'n_neighbors': [1, 3, 5, 7, 9, 11],
                'weights': ['uniform', 'distance'],
                # 'p': [1, 2], # power untuk distance. 1=manhattan, 2=euclidean
                'metric': ['euclidean', 'manhattan', 'minkowski'],
                'n_jobs': [-1]
             }

clf = KNeighborsClassifier()
'''

# Decision Tree
'''
parameters = {
                'criterion': ['gini', 'entropy'], # di buku, katanya kurang lebih sama
                'min_samples_leaf': [1, 10, 20],
                # 'min_weight_fraction_leaf': [0.0, 0.01, 0.1],
                'max_depth': [3, 6, 9, 12],
                'class_weight': [None, 'balanced'],
                'max_features': [None, 'sqrt', 'log2'],
                'presort': [True],
                'random_state': [0]
             }

clf = tree.DecisionTreeClassifier()
'''

# Random Forest
# oob_score gak perlu test set, kalo size test & trainingnya sama
'''
parameters = {
                'n_estimators': [100],
                'oob_score': [True], # untuk scoring saja, gak ngefek ke performance
                # 'criterion': ['gini', 'entropy'], # di buku, katanya kurang lebih sama
                'min_samples_leaf': [1, 3, 5],
                # 'min_weight_fraction_leaf': [0.0, 0.01, 0.1],
                'max_depth': [3, 6, 9],
                'class_weight': [None, 'balanced'],
                'max_features': [None, 'sqrt', 'log2'],
                'n_jobs': [-1],
                'random_state': [0]
             }

clf = RandomForestClassifier()
'''

# XGBoost
'''
parameters = {
                'n_estimators': [100], # default 100
                'max_depth': [3, 6, 9],
                'gamma': [0, 0.1, 0.3, 1],
                'subsample': [0.8, 0.9, 1],
                'reg_alpha' : [0, 0.1, 0.3, 1], # L1 regularization, default 0
                # 'reg_lambda': [0, 0.1, 0.3, 1], # L2 regularization, default 1
                # 'learning_rate': [0.1, 0.3], # biar lebih cepat
                'nthread': [-1],
                'seed': [0]
             }


clf = XGBClassifier()
'''

# Perceptron
'''
parameters = {
                'penalty': [None, 'l1', 'l2', 'elasticnet'],
                'alpha': [0.0001, 0.001],
                'max_iter': [5, 500, 1000],
                'class_weight': [None, 'balanced'],
                'n_jobs': [-1],
                'random_state': [0]
             }

clf = Perceptron()
'''

# Neural Network
parameters = {
                # 'hidden_layer_sizes': [{}],
                'activation': ['relu', 'logistic', 'tanh'], # default relu
                'solver': ['lbfgs', 'sgd'],
                'alpha': [0.0, 0.0001, 0.001, 0.01], # l2 penalty, default 0.0001
                'learning_rate': ['constant', 'adaptive'], # only for sgd solver
                # learning_rate_init
                'max_iter': [200],
                'random_state': [0]
             }

clf = MLPClassifier()

grid_obj = GridSearchCV(clf, parameters, cv=10, scoring='accuracy') # n_jobs=-1 error
grid_obj = grid_obj.fit(X, y)

print(grid_obj.best_estimator_)
print('Best GridSearchCV Score : ' + str(grid_obj.best_score_))

# Set the clf to the best combination of parameters
clf = grid_obj.best_estimator_

# Fit the best algorithm to the data.
clf.fit(X, y)

# score_train = clf.score(X, y) # perlu? bener?
# print('Training Accuracy : ' + str(score_train))

# ganti clf jd grid_obj untuk nested CV, taro setelah grid_obj.fit
# scores_log = cross_val_score(clf, X, y, cv=10)
# print('Final CV accuracy: %.3f +/- %.3f' % (np.mean(scores_log), np.std(scores_log)))

# score_test = clf.score(X_test, y_test)
y_pred_test = clf.predict(X_test)

print('Test Accuracy : ' + str(accuracy_score(y_test, y_pred_test)))
print('Test F1 Score : ' + str(f1_score(y_test, y_pred_test)))

if isinstance(clf, RandomForestClassifier):
    print("OOB Score : " + str(clf.oob_score_))

# if isinstance(clf, tree.DecisionTreeClassifier):
    # print_decision_tree(clf, X)

# pastiin underfit/overfit stelah dpt best param
# gen_learning_curve(clf, X, y)

# gen validation curve hanya untuk contoh,
# harusnya sblm dpt best param
# gen_validation_curve(clf, X, y)

# use selected features
# test_filtered = test.loc[:, cols].fillna(method='pad')
# y_pred = clf.predict(test_filtered)

# use all features
# y_pred = clf.predict(test)

# submit_kaggle(test.loc[:,'PassengerId'], y_pred)