# region import
from sklearn.ensemble import BaggingClassifier
from sklearn.neighbors import KNeighborsClassifier
import sys
sys.path.append("YOUR_PATH_OS1/codes")
from ModelPreparation import model_preparation
from SubmitKaggle import submit_kaggle
# endregion

train, test, X, y, X_test, y_test, cols = model_preparation()

bagging = BaggingClassifier(
    KNeighborsClassifier(
        n_neighbors=2,
        weights='distance'
        ),
    oob_score=True,
    max_samples=0.5,
    max_features=1.0
    )
clf_bag = bagging.fit(X,y)
score_bag = clf_bag.oob_score_
print('Score Training OOB Bagging KNN : ' + str(score_bag))

score_bag_test = clf_bag.score(X_test,y_test)
print('Score Test OOB Bagging KNN : ' + str(score_bag_test))

# use the chosen features on the test/prediction set
test_final = test.loc[:,cols].fillna(method='pad')
y_pred = clf_bag.predict(test_final)

submit_kaggle(test.loc[:,'PassengerId'], y_pred)
# endregion