# region imports
import pandas as pd
import numpy as np
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import make_scorer, accuracy_score
from sklearn.model_selection import GridSearchCV, cross_val_score
from sklearn.metrics import f1_score
import sys
sys.path.append("YOUR_PATH_OS1/codes")
sys.path.append("YOUR_PATH_OS2/codes")
from ModelPreparation import model_preparation
from GenLearningCurve import gen_learning_curve
from GenValidationCurve import gen_validation_curve
from SubmitKaggle import submit_kaggle
# endregion

parameters = {
                'max_iter': [1000],
                'penalty': ['l1', 'l2'],
                'C': [0.1, 1, 3, 10, 100],
                # 'n_jobs': [-1], #liblinear gk ngefek
                'random_state': [0]
             }

train, test, X, y, X_test, y_test, cols = model_preparation()

# fit with all data
X = pd.concat([X,X_test])
y = pd.concat([y,y_test])

clf_log = LogisticRegression()

grid_obj = GridSearchCV(clf_log, parameters, cv=5, scoring='accuracy', n_jobs=-1)
grid_obj = grid_obj.fit(X, y) # pake ini?

scores_log = cross_val_score(grid_obj, X, y, cv=10)
print('Final CV accuracy: %.3f +/- %.3f' % (np.mean(scores_log), np.std(scores_log)))

print(grid_obj.best_estimator_)
print('Best GridSearchCV Score : ' + str(grid_obj.best_score_))

# Set the clf to the best combination of parameters
clf_log = grid_obj.best_estimator_

# Fit the best algorithm to the data.
clf_log.fit(X, y)

# score_log_test = clf_log.score(X_test,y_test)
# print('Test Accuracy : ' + str(score_log_test))

# y_pred_test = clf_log.predict(X_test)
# print('Test F1 Score : ' + str(f1_score(y_test, y_pred_test)))

# pastiin underfit/overfit stelah dpt best param
# gen_learning_curve(clf_log, X, y)

# gen validation curve hanya untuk contoh,
# harusnya sblm dpt best param
# gen_validation_curve(clf_log, X, y)

# use selected features
# test_filtered = test.loc[:, cols].fillna(method='pad')
# y_pred = clf.predict(test_filtered)

# use all features
y_pred = clf_log.predict(test)

submit_kaggle(test.loc[:,'PassengerId'], y_pred)