# region imports
import pandas as pd
import numpy as np
from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC
from sklearn.ensemble import RandomForestClassifier, VotingClassifier
from sklearn import tree
from xgboost.sklearn import XGBClassifier
from sklearn.metrics import make_scorer, accuracy_score, f1_score
from sklearn.model_selection import GridSearchCV, cross_val_score
import sys
sys.path.append("YOUR_PATH_OS1/codes")
sys.path.append("YOUR_PATH_OS2/codes")
from ModelPreparation import model_preparation
from SubmitKaggle import submit_kaggle
# endregion

"""Perform Soft & Hard Majority Voting by pre-defined best hyperparameters of each classifier
(not necessarily the best hyperparameters for Bagging)"""

train, test, X, y, X_test, y_test, cols = model_preparation()

X = pd.concat([X,X_test])
y = pd.concat([pd.DataFrame(y),pd.DataFrame(y_test)])

# region best_param_with_all_features
best_param_logreg = {
                'max_iter': 1000,
                'penalty': 'l2',
                'C': 0.1,
                'random_state': 0
             }

best_param_RF = {
                'n_estimators': 100,
                # 'criterion': 'entropy',
                'oob_score': True, # untuk scoring saja, gak ngefek ke performance
                'min_samples_leaf': 3,
                # 'min_weight_fraction_leaf': [0.0, 0.01, 0.1],
                'max_depth': 6,
                'class_weight': 'balanced',
                'max_features': 'log2',
                'n_jobs': -1,
                'random_state': 0
             }

best_param_SVM = {
                'C': 100,
                'gamma': 0.001,
                'probability': True,
                'random_state': 0
             }

best_param_dec_tree = {'max_depth': 6,
                      'max_features': 'sqrt',
                      'presort': [True],
                      'random_state': 0
                       }

best_param_XGB = {
                'n_estimators': 100, # default 100
                'max_depth': 3,
                'gamma': 0,
                'subsample': 0.8,
                'reg_alpha': 1, # L1 regularization, default 0
                'reg_lambda': 1, # L2 regularization, default 1
                # 'learning_rate': [0.1, 0.3], # biar lebih cepat
                'nthread': -1,
                'seed': 0
             }
# endregion

# region best_param_RFE_top_10

best_param_logreg_RFE = {
                'max_iter': 1000,
                'penalty': 'l1',
                'C': 1,
                'random_state': 0
             }

best_param_RF_RFE = {
                'n_estimators': 100,
                # 'criterion': 'entropy',
                'oob_score': True, # untuk scoring saja, gak ngefek ke performance
                'min_samples_leaf': 3,
                # 'min_weight_fraction_leaf': [0.0, 0.01, 0.1],
                'max_depth': 6,
                'class_weight': 'balanced',
                'max_features': 'log2',
                'n_jobs': -1,
                'random_state': 0
             }

best_param_SVM_RFE = {
                'C': 1,
                'gamma': 0.1,
                'class_weight': None,
                'probability': True,
                'random_state': 0
             }

best_param_dec_tree_RFE = {'max_depth': 6,
                      'max_features': None,
                      'presort': [True],
                      'random_state': 0
                       }

best_param_XGB_RFE = {
                'n_estimators': 100, # default 100
                'max_depth': 3,
                'gamma': 0,
                'subsample': 0.8,
                'reg_alpha': 1, # L1 regularization, default 0
                'reg_lambda': 1, # L2 regularization, default 1
                # 'learning_rate': [0.1, 0.3], # biar lebih cepat
                'nthread': -1,
                'seed': 0
             }
# endregion

clf_logreg = LogisticRegression(**best_param_logreg_RFE)
clf_svm = SVC(**best_param_SVM_RFE)
clf_dec_tree = tree.DecisionTreeClassifier(**best_param_dec_tree_RFE)
clf_rf = RandomForestClassifier(**best_param_RF_RFE)
clf_xgb = XGBClassifier(**best_param_XGB_RFE)

clf_voting_soft = VotingClassifier([('logreg', clf_logreg),
                                    ('svm', clf_svm),
                                    ('tree', clf_dec_tree),
                                    ('rf', clf_rf),
                                    ('xgb', clf_xgb)],
                                    voting='soft',
                                    n_jobs=-1)

clf_voting_hard = VotingClassifier([('logreg', clf_logreg),
                                    ('svm', clf_svm),
                                    ('tree', clf_dec_tree),
                                    ('rf', clf_rf),
                                    ('xgb', clf_xgb)],
                                    voting='hard',
                                    n_jobs=-1)

clf_voting_soft.fit(X, y)

clf_voting_hard.fit(X, y)

# print('Soft Score : ' + str(clf_voting_soft.score(X, y)))

# print('Hard Score : ' + str(clf_voting_hard.score(X, y)))

# print('Soft Test Score : ' + str(clf_voting_soft.score(X_test, y_test)))

# print('Hard Test Score : ' + str(clf_voting_hard.score(X_test, y_test)))

# Hard acc test score lebih bagus (generalization lebih bagus)

# use selected features
test_filtered = test.loc[:, cols].fillna(method='pad')
y_pred = clf_voting_hard.predict(test_filtered)

# use all features
# y_pred = clf_voting_hard.predict(test)

submit_kaggle(test.loc[:,'PassengerId'], y_pred)