# region imports
import pandas as pd
import numpy as np
from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC
from sklearn.metrics import make_scorer, accuracy_score
from sklearn.model_selection import GridSearchCV, cross_val_score
from sklearn.metrics import f1_score
import sys
sys.path.append("YOUR_PATH_OS1/codes")
sys.path.append("YOUR_PATH_OS2/codes")
from ModelPreparation import model_preparation
from GenLearningCurve import gen_learning_curve
from GenValidationCurve import gen_validation_curve
from SubmitKaggle import submit_kaggle
# endregion

train, test, X, y, X_test, y_test, cols = model_preparation()

# fit with all data
# X = pd.concat([X,X_test])
# y = pd.concat([y,y_test])

# logreg
'''
parameters = {
                'max_iter': [1000],
                'penalty': ['l1', 'l2'],
                'C': [0.1, 1, 3, 10, 100],
                # 'n_jobs': [-1], #liblinear gk ngefek
                'random_state': [0]
             }

clf = LogisticRegression()
'''

# svm
# best : C=3000, gamma=6e-06 -> Test Acc 0.804469273743, tp Kaggle 0.51674?
parameters = {
                'C': [3000, 3500, 4000, 4500, 5000], # makin kecil, makin besar margin
                'gamma': [0.000001, 0.000003, 0.000006],
                'random_state': [0]
             }

clf = SVC()

grid_obj = GridSearchCV(clf, parameters, cv=5, scoring='accuracy')
# pake ini? Kalo gk pake gk bisa dpt best_estimator_
grid_obj = grid_obj.fit(X, y)

scores_log = cross_val_score(grid_obj, X, y, cv=10)
print('Final CV accuracy: %.3f +/- %.3f' % (np.mean(scores_log), np.std(scores_log)))

print(grid_obj.best_estimator_)
print('Best GridSearchCV Score : ' + str(grid_obj.best_score_))

# Set the clf to the best combination of parameters
clf = grid_obj.best_estimator_

# Fit the best algorithm to the data.
clf.fit(X, y)

score_train = clf.score(X, y)
print('Training Accuracy : ' + str(score_train))

score_test = clf.score(X_test, y_test)
print('Test Accuracy : ' + str(score_test))

# y_pred_test = clf_log.predict(X_test)
# print('Test F1 Score : ' + str(f1_score(y_test, y_pred_test)))

# pastiin underfit/overfit stelah dpt best param
gen_learning_curve(clf, X, y)

# gen validation curve hanya untuk contoh,
# harusnya sblm dpt best param
# gen_validation_curve(clf, X, y)

# use selected features
# test_filtered = test.loc[:, cols].fillna(method='pad')
# y_pred = clf.predict(test_filtered)

# use all features
y_pred = clf.predict(test)

submit_kaggle(test.loc[:,'PassengerId'], y_pred)