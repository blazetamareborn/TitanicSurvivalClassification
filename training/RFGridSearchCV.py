import pandas as pd
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import make_scorer, accuracy_score
from sklearn.model_selection import GridSearchCV
import sys
sys.path.append("YOUR_PATH_OS1/codes")
from ModelPreparation import model_preparation
from SubmitKaggle import submit_kaggle

train, test, X, y, X_test, y_test, cols = model_preparation()

# Choose some parameter combinations to try

parameters = {'n_estimators': [100, 200, 500],
              'max_features': ['log2', 'sqrt','auto'],
              'criterion': ['entropy', 'gini'],
              # 'max_depth': [2, 3, 5, 10],
              # 'min_samples_split': [2, 3, 5],
              'min_samples_leaf': [1,5,10],
              'oob_score': [True],
              'n_jobs': [-1],
              'random_state': [0]
             }


'''
print(grid_obj.best_estimator_)

RandomForestClassifier(bootstrap=True, class_weight=None, criterion='entropy',
            max_depth=None, max_features='log2', max_leaf_nodes=None,
            min_impurity_decrease=0.0, min_impurity_split=None,
            min_samples_leaf=5, min_samples_split=2,
            min_weight_fraction_leaf=0.0, n_estimators=500, n_jobs=-1,
            oob_score=True, random_state=None, verbose=True,
            warm_start=False)
'''

# try fit with all data
X = pd.concat([X,X_test])
y = pd.concat([y,y_test])

# Choose the type of classifier.
clf = RandomForestClassifier(verbose=True)

# Type of scoring used to compare parameter combinations
acc_scorer = make_scorer(accuracy_score)

# Run the grid search
# Default 3 k-fold CV
grid_obj = GridSearchCV(clf, parameters, scoring=acc_scorer)
grid_obj = grid_obj.fit(X, y)

# Set the clf to the best combination of parameters
clf = grid_obj.best_estimator_

# Fit the best algorithm to the data.
clf.fit(X, y)

acc_train_clf = round(clf.score(X, y) * 100, 2)
# acc_cv_clf = round(clf.score(X_test, y_test) * 100, 2)

print(grid_obj.best_estimator_)
print('Acc Training : ' + str(acc_train_clf))
# print('Acc CV : ' + str(acc_cv_clf))

# use selected features
# test_filtered = test.loc[:, cols].fillna(method='pad')
# y_pred = clf.predict(test_filtered)

# use all features
y_pred = clf.predict(test)

submit_kaggle(test.loc[:,'PassengerId'], y_pred)
# endregion