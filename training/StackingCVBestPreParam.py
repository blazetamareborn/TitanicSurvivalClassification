# region imports
import pandas as pd
import numpy as np
from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC
from sklearn.ensemble import RandomForestClassifier
from sklearn import tree
from xgboost.sklearn import XGBClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import make_scorer, accuracy_score, f1_score
from sklearn.model_selection import GridSearchCV, cross_val_score
from mlxtend.classifier import StackingCVClassifier
import sys
sys.path.append("YOUR_PATH_OS1/codes")
sys.path.append("YOUR_PATH_OS2/codes")
from ModelPreparation import model_preparation
from SubmitKaggle import submit_kaggle
# endregion

"""Perform Stacking with CV by pre-defined best hyperparameters of each classifier
(not necessarily the best hyperparameters for StackingCV)
more info : https://rasbt.github.io/mlxtend/user_guide/classifier/StackingCVClassifier, example 3"""

RANDOM_SEED = 1

train, test, X, y, X_test, y_test, cols = model_preparation()

# X = pd.concat([X,X_test])
# y = pd.concat([y,y_test])

# y = y.iloc[:,0] # harus selalu ada

# region best_param_with_all_features
best_param_logreg = {
                'max_iter': 1000,
                'penalty': 'l2',
                'C': 0.1,
                'random_state': 0
             }

best_param_RF = {
                'n_estimators': 100,
                # 'criterion': 'entropy',
                'oob_score': True, # untuk scoring saja, gak ngefek ke performance
                'min_samples_leaf': 3,
                # 'min_weight_fraction_leaf': [0.0, 0.01, 0.1],
                'max_depth': 6,
                'class_weight': 'balanced',
                'max_features': 'log2', #
                'n_jobs': -1,
                'random_state': 0
             }

best_param_SVM = {
                'C': 100,
                'gamma': 0.001,
                'probability': True,
                'random_state': 0
             }

best_param_dec_tree = {'max_depth': 6,
                      'max_features': 'sqrt',
                      'presort': [True],
                      'random_state': 0
                       }

best_param_XGB = {
                'n_estimators': 100, # default 100
                'max_depth': 3,
                'gamma': 0,
                'subsample': 0.8,
                'reg_alpha': 1, # L1 regularization, default 0
                'reg_lambda': 1, # L2 regularization, default 1
                # 'learning_rate': [0.1, 0.3], # biar lebih cepat
                'nthread': -1,
                'seed': 0
             }
# endregion

# region best_param_RFE_top_10

best_param_logreg_RFE = {
                'max_iter': 1000,
                'penalty': 'l1',
                'C': 1,
                'random_state': 0
             }

best_param_RF_RFE = {
                'n_estimators': 100,
                # 'criterion': 'entropy',
                'oob_score': True, # untuk scoring saja, gak ngefek ke performance
                'min_samples_leaf': 3,
                # 'min_weight_fraction_leaf': [0.0, 0.01, 0.1],
                'max_depth': 6,
                'class_weight': 'balanced',
                'max_features': 'log2', #
                'n_jobs': -1,
                'random_state': 0
             }

best_param_SVM_RFE = {
                'C': 1,
                'gamma': 0.1,
                'class_weight': None,
                'probability': True,
                'random_state': 0
             }

best_param_dec_tree_RFE = {'max_depth': 6,
                      'max_features': None,
                      'presort': [True],
                      'random_state': 0
                       }

best_param_XGB_RFE = {
                'n_estimators': 100, # default 100
                'max_depth': 3,
                'gamma': 0,
                'subsample': 0.8,
                'reg_alpha': 1, # L1 regularization, default 0
                'reg_lambda': 1, # L2 regularization, default 1
                # 'learning_rate': [0.1, 0.3], # biar lebih cepat
                'nthread': -1,
                'seed': 0
             }

best_param_KNN_RFE = {
                'n_neighbors': 9,
                'weights': 'distance',
                'metric': 'euclidean',
                'n_jobs': -1
             }

# endregion

clf_logreg = LogisticRegression(**best_param_logreg_RFE)
clf_svm = SVC(**best_param_SVM_RFE)
clf_dec_tree = tree.DecisionTreeClassifier(**best_param_dec_tree_RFE)
clf_rf = RandomForestClassifier(**best_param_RF_RFE)
clf_xgb = XGBClassifier(**best_param_XGB_RFE)
clf_knn = KNeighborsClassifier(**best_param_KNN_RFE)

np.random.seed(RANDOM_SEED) # pengganti random_state
clf_stacking_cv = StackingCVClassifier(classifiers=[clf_dec_tree, clf_rf, clf_xgb],
                                       meta_classifier=clf_logreg,
                                       cv=5) # do CV on 1st level classifiers

scores = cross_val_score(clf_stacking_cv, X.values, y.values, cv=3, scoring='accuracy')


print('CV Accuracy Mean : ' + str(scores.mean()) + ', STD : ' + str(scores.std()))

clf_stacking_cv.fit(X.values, y.values)
y_pred_test = clf_stacking_cv.predict(X_test.values)

print('Test Accuracy : ' + str(accuracy_score(y_test, y_pred_test)))
print('Test F1 Score : ' + str(f1_score(y_test, y_pred_test)))

# use selected features
test_filtered = test.loc[:, cols].fillna(method='pad')
y_pred = clf_stacking_cv.predict(test_filtered.values)

# use all features
# y_pred = clf_voting_hard.predict(test)

submit_kaggle(test.loc[:,'PassengerId'], y_pred)